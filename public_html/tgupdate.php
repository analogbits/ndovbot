<?php

/*
 * Copyright (C) 2016 analogbits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

class tgupdate {
    
    private $json;
    public $updateType;
    
    function __construct() {
        $post = file_get_contents('php://input');
        $this->full = $post;
        $this->json = json_decode($post, true);
        // $dbg = new abdbg();
        // $dbg->logresponse('query', $post);
        if (array_key_exists('inline_query', $this->json)) $this->updateType = "inline_query";
        if (array_key_exists('message', $this->json)) $this->updateType = "message";
        if (array_key_exists('callback_query', $this->json)) $this->updateType = "callback_query";
        if (array_key_exists('chosen_inline_result', $this->json)) $this->updateType = "chosen_inline_result";
    }
    
    function getInlinequery(){
        if ($this->updateType != 'inline_query') throw new Exception ("getInlinequery called on non-inlinequery object");
        return ['id' => $this->json['inline_query']['id'], 'query' =>$this->json['inline_query']['query'], 'user' => $this->json['inline_query']['from']['id']];
    }
    
    function getInlineResult(){
        $tmp1 = $this->json['chosen_inline_result'];
        return [
            'user' => $tmp1['from']['id'],
            'stop' => $tmp1['result_id']
        ];
    }
    
    function getCallbackQuery(){
        return ['id' => $this->json['callback_query']['inline_message_id'], 'cid' => $this->json['callback_query']['id'], 'data' => $this->json['callback_query']['data']];
    }

    function getMessage(){
        if ($this->updateType != 'message') throw new Exception ("getMessage called on non-message object");
        return ['id' => $this->json['message']['message_id'], 'text' =>$this->json['message']['text'], 'chat_id' =>$this->json['message']['chat_id']];
    }
    function parse_args($message, $command) {
        if (0 === strpos($message, $command . " ")) {
             // Starts with /command. Without bot username.
             return substr($message, 0, strlen($command . " ") - 1);
        } elseif (0 === strpos($message, $command . "@ndovbot ")) {
             // Starts with /command@ndovbot. 
             return substr($message, 0, strlen($command . "@ndovbot ") - 1);
        } else {
             // Couldn't parse successfully.
             return false;
        }
    }
}

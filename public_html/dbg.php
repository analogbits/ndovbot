<?php

/*
 * Copyright (C) 2016 analogbits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

class abdbg{
    
    private $debugenabled;
    
    function __construct() {
        $cfg = new config();
        $this->debugenabled = $cfg->debug;
    }
    
    function log($msg){
        if ($this->debugenabled){
            file_put_contents('data/bot.log', time() . ':' . $msg . "\n", FILE_APPEND);
        }
    }
    
    function logresponse($tag, $rep){
        if ($this->debugenabled){
            $time = time();
            file_put_contents('data/sr_' . $time . '.log', $rep);
            file_put_contents('data/bot.log', $time . '[' . $tag . ']: Server response logged in sr_' . $time .".log\n", FILE_APPEND);
        }
    }
}
<?php

/*
 * Copyright (C) 2016 analogbits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

class SortMdArray {
    public $sort_order = 'asc'; // default
    public $sort_key = 'timetodepart'; // default

    public function sortByKey(&$array) {
        usort($array, array(__CLASS__, 'sortByKeyCallback'));
    }

    function sortByKeyCallback($a, $b) {
        if($this->sort_order == 'asc') {
            $return = $a[$this->sort_key] - $b[$this->sort_key];
        } else if($this->sort_order == 'desc') {
            $return = $b[$this->sort_key] - $a[$this->sort_key];
        }
        return $return;
    }
}


class timingpoints {

    private $tps = [];
    private $ps = [];

    private function getpasses() {
        $passes = [];
        foreach ($this->tps as $tp) {
            foreach ($tp['Passes'] as $pass) {
                array_push($passes, $pass);
            }
        }
        return $passes;
    }

    private function getemoji($type){
        switch ($type){
            case 'BUS':
                return "\u{1F68C}";
            case 'TRAM':
                return "\u{1F68B}";
            case 'METRO':
                return "\u{1F687}";
            case 'TRAIN':
                return "\u{1F684}";
        }
        return '';
    }

    function __construct($tps) {
        foreach ($tps as $timingpoint) {
            array_push($this->tps, $timingpoint);
        }
        $this->ps = $this->getpasses();
    }

    function getlines() {
        $passes = $this->ps;
        $lines = [];
        foreach ($passes as $pass) {
            $lines[$pass['LinePlanningNumber']] = $pass['LinePublicNumber'];
        }
        return $lines;
    }

    function getdepartures() {
        $passes = $this->ps;
        $deptimes = [];
        foreach ($passes as $pass) {
            $deptime = strtotime($pass['ExpectedDepartureTime']);
            $currtime = time();
            $reltime = round(($deptime - $currtime) / 60, 0, PHP_ROUND_HALF_DOWN);
            $destname = $pass['DestinationName50'];
            $line = $pass['LinePublicNumber'];
            $type = $pass['TransportType'];
            array_push($deptimes, [
                'timetodepart' => $reltime,
                'destination' => $destname,
                'line' => $line,
                'emoji' => $this->getemoji($type)
            ]);
        }
        $sort = new SortMdArray;
        $sort->sortByKey($deptimes);
        return $deptimes;
    }

}

class kv78turbo {

    private $cfg;

    function __construct() {
        $this->cfg = new config();
    }

    function updateStopareas() {
        $con = curl_init($this->cfg->kv78api . 'stopareacode');
        curl_setopt_array($con, [
            CURLOPT_USERAGENT => 'ABTelegramBot/1.0 (+support@analogbits.me; updating stopareas)',
            CURLOPT_TIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => 1
        ]);
        $ret = curl_exec($con);
        if ($ret == FALSE)
            throw new Exception("Cannot fetch stoparea codes");
        else
            file_put_contents('data/stopareacodes.json', $ret);
    }

    function findstops($name) {
        $stops = json_decode(file_get_contents('data/stopareacodes.json'), true);
        $keys = array_filter($stops, function($el) use ($name) {
            return (strpos(strtolower($el['TimingPointName']), strtolower($name)) !== false);
        });
        return $keys;
    }

    function getstopname($stopareacode){
      $stops = json_decode(file_get_contents('data/stopareacodes.json'), true);
      return $stops[$stopareacode]['TimingPointName'];
    }

    function getfullstop($stopareacode){
      $stops = json_decode(file_get_contents('data/stopareacodes.json'), true);
      return $stops[$stopareacode];
    }

    function getdepartures($stops) {
        $st = '';
        foreach ($stops as $keyname => $stop) {
            $st .= $keyname . ',';
        }
        $con = curl_init($this->cfg->kv78api . 'stopareacode/' . $st);
        curl_setopt_array($con, [
            CURLOPT_USERAGENT => 'ABTelegramBot/1.0 (+support@analogbits.me)',
            CURLOPT_TIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => 1
        ]);
        $ret = curl_exec($con);
        if ($ret == false)
            throw new Exception("Cannot fetch departure times - " . $this->cfg->kv78api);
        $recv = json_decode($ret, true);
        $departures = ['lines' => [], 'departures' => []];
        foreach ($recv as $linecode => $value) {
            $timingpoints = new timingpoints($value);
            $lines = $timingpoints->getlines();
            $deps = $timingpoints->getdepartures();
            $departures['lines'][$linecode] = $lines;
            $departures['departures'][$linecode] = $deps;
        }
        return $departures;
    }

}

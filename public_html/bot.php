<?php

/*
 * Copyright (C) 2016 analogbits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once 'config.php';
require_once 'tgupdate.php';
require_once 'kv78turbo.php';
require_once 'dbg.php';

$cfg = new config();
$req = new tgupdate();
$ov = new kv78turbo();
$dbg = new abdbg();

date_default_timezone_set("Europe/Amsterdam");

class recentquerys {

    private $enabled;
    private $redis;
    private $ov;

    public function __construct() {
        $cfg = new config();
        $this->enabled = $cfg->redis;
        if ($this->enabled) {
            $this->redis = new Redis();
            $this->redis->pconnect('127.0.0.1');
            $this->ov = new kv78turbo();
        }
    }

    public function saveRecentQuery($q) {
        if ($this->enabled) {
          $stop = $this->ov->getfullstop($q['stop']);
            $usrstr = $this->redis->get(strval($q['user']));
            if (!$usrstr) {
                $this->redis->set(strval($q['user']), serialize([$q['stop'] => $stop]));
            } else {
                $arr = unserialize($usrstr);
                $arr[$q['stop']] = $stop;
                array_unique($arr);
                $this->redis->set(strval($q['user']), serialize($arr));
            }
        }
    }

    public function getRecentQuery($user) {
        if ($this->enabled) {
            return unserialize($this->redis->get(strval($user)));
        } else {
            return false;
        }
    }

}

if ($req->updateType == 'inline_query') {
    if (trim($req->getInlinequery()['query']) == '' && !$cfg->redis)
        exit(0);
    $rds = new recentquerys();
    $q = $req->getInlinequery();
    if ($q['query'] != '') {
        $k = $ov->findstops($q['query']);
    } else {
        $k = $rds->getRecentQuery($q['user']);
        $dbg->logresponse('redisrtrv', serialize($k));
    }
    $dbg->log('inline query:' . $q['query']);
    $d = $ov->getdepartures($k);
    $a = array();
    foreach ($k as $vk => $v) {
        $desc = 'Lines: ';
        foreach ($d['lines'][$vk] as $ln) {
            $desc .= $ln . ' ';
        }
        $msg = sprintf("Departures for %s\n", $v['TimingPointName']);
        foreach ($d['departures'][$vk] as $dep) {
            if ($dep['timetodepart'] > 15 || $dep['timetodepart'] < 0)
                break;
            $msg .= sprintf("%s %s | %s - %g min\n", $dep['emoji'], $dep['line'], $dep['destination'], $dep['timetodepart']);
        }
        if (count($a) != 49)
            array_push($a, [
                'type' => 'article',
                'id' => strval($vk),
                'title' => $v['TimingPointTown'] . ', ' . $v['TimingPointName'],
                'input_message_content' => ['message_text' => $msg, 'parse_mode' => 'Markdown'],
                'description' => $desc
            ]);
    }
    foreach ($a as $key => $av) { // <= shitty workaround for a bug in PHP7, sorry for the shitty code here
        $keys = array();
        array_push($keys, [['text' => 'Refresh',
        'callback_data' => $av['id']]]);
        $keyboard = ['inline_keyboard' => $keys];
        $a[$key]['reply_markup'] = $keyboard;
    }
    $rep = [
        'inline_query_id' => $q['id'],
        'results' => $a,
        'cache_time' => 30
    ];
    $jsn = json_encode($rep);
    $con = curl_init('https://api.telegram.org/bot' . $cfg->token . '/answerInlineQuery');
    curl_setopt_array($con, [
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $jsn,
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsn)
        ]
    ]);
    curl_exec($con);
}

if ($req->updateType == 'chosen_inline_result') {
    $q = $req->getInlineResult();
    $sto = new recentquerys();
    $sto->saveRecentQuery($q);
    $dbg->log('inline query result for user ', $q['user'] . ':' . $q['stop']);
}

if ($req->updateType == 'callback_query') {
    $q = $req->getCallbackQuery();
    $stop = $q['data'];
    $dbg->log('refresh requested for stop ' . $q['data']);
    $d = $ov->getdepartures(array($stop => 'kek'));
    $stopname = $ov->getstopname($stop);
    $msg = sprintf("Departures for %s\n", $stopname);
    foreach ($d['departures'][$stop] as $dep) {
        if ($dep['timetodepart'] > 15)
            break;
        $msg .= sprintf("%s %s | %s - %g min\n", $dep['emoji'], $dep['line'], $dep['destination'], $dep['timetodepart']);
    }

    $keys = array();
        array_push($keys, [['text' => 'Refresh',
        'callback_data' => $stop]]);
        $keyboard = ['inline_keyboard' => $keys];

    $answer = [
        'inline_message_id' => $q['id'],
        'text' => $msg,
        'reply_markup' => $keyboard
    ];
    $jsn = json_encode($answer);
    $con = curl_init('https://api.telegram.org/bot' . $cfg->token . '/editMessageText');
    curl_setopt_array($con, [
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $jsn,
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsn)
        ]
    ]);
    curl_exec($con);
    $jsn = json_encode(['callback_query_id' => $q['cid']]);
    $con = curl_init('https://api.telegram.org/bot' . $cfg->token . '/answerCallbackQuery');
    curl_setopt_array($con, [
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $jsn,
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsn)
        ]
    ]);
    curl_exec($con);
}

if ($req->updateType == 'message') {
    if (trim($req->getMessage()['message']) == '')
        exit(0);
    $q = $req->getMessage();
    $k = $ov->findstops($q['message']);
    if (parse_args($k, "/stops") === false) {
        $a = array();
        $msg = print("Error: invalid argrument.");
        array_push($a, [
            'chat_id' => $q['chat_id'],
            'text' => $msg,
            'parse_mode' => 'Markdown',
            'reply_to_message_id' => $q['message_id']
        ]);
    } else {
        $query = parse_args($k, "/stops");
        $d = $ov->getdepartures($query);
        $a = array();
        foreach ($k as $vk => $v) {
            $desc = 'Lines: ';
            foreach ($d['lines'][$vk] as $ln) {
                $desc .= $ln . ' ';
            }
            $msg = sprintf("Departures for %s\n", $v['TimingPointName']);
            foreach ($d['departures'][$vk] as $dep) {
                if ($dep['timetodepart'] > 15)
                    break;
                $msg .= sprintf("%s %s | %s - %g min\n", $dep['emoji'], $dep['line'], $dep['destination'], $dep['timetodepart']);
            }
            if (count($a) != 49)
                array_push($a, [
                    'chat_id' => $q['chat_id'],
                    'text' => $msg,
                    'parse_mode' => 'Markdown',
                    'reply_to_message_id' => $q['message_id']
                ]);
        }
    }
    $jsn = json_encode($a);
    file_put_contents('json.json', $jsn);
    $con = curl_init('https://api.telegram.org/bot' . $cfg->token . '/sendMessage');
    curl_setopt_array($con, [
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $jsn,
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsn)
        ]
    ]);
    curl_exec($con);
}

<?php

/* 
 * Copyright (C) 2016 analogbits
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once 'config.php';
require_once 'kv78turbo.php';

$cfg = new config();
$req = [
    'url' => 'https://' . $cfg->fqdn . '/bot.php'
];
$jsn = json_encode($req);
$con = curl_init('https://api.telegram.org/bot' . $cfg->token . '/setWebhook');
curl_setopt($con, CURLOPT_CUSTOMREQUEST, "POST"); 
curl_setopt($con, CURLOPT_POSTFIELDS, $jsn);
curl_setopt($con, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',
    'Content-Length: ' . strlen($jsn))                                                                       
);  
curl_exec($con);

$ov = new kv78turbo();
$ov->updateStopareas();
